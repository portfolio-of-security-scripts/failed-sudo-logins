#! /bin/bash

after_month=$1
after_day=$2
after_year=$3
after_timestamp=$4
epoch_seconds_after=$(date -d "$after_month $after_day $after_year $after_timestamp" +%s)

sudo aureport -au | grep sudo | grep no > aureport.txt

while read line; do
  date=$(echo $line | awk '{print $2}') 
  month_num=$(echo $date | cut -d/ -f1)
  day=$(echo $date | cut -d/ -f2)
  year=$(echo $date | cut -d/ -f3)
  timestamp=$(echo $line | awk '{print $3}')  
 
  if [[ "$month_num" == "01" ]]; then
    month="Jan"
  elif [[ "$month_num" == "02" ]]; then
    month="Feb"
  elif [[ "$month_num" == "03" ]]; then
    month="March"
  elif [[ "$month_num" == "04" ]]; then 
    month="April"
  elif [[ "$month_num" == "05" ]]; then
    month="May"
  elif [[ "$month_num" == "06" ]]; then
    month="June"
  elif [[ "$month_num" == "06" ]]; then
    month="July"
  elif [[ "$month_num" == "06" ]]; then
    month="Aug"
  elif [[ "$month_num" == "06" ]]; then
    month="Sept"
  elif [[ "$month_num" == "06" ]]; then
    month="Oct"
  elif [[ "$month_num" == "06" ]]; then
    month="Nov"
  elif [[ "$month_num" == "06" ]]; then
    month="Dec"
  fi

epoch_seconds_log_entry=$(date -d "$month $day $year $timestamp" +%s)
if [[ "$epoch_seconds_log_entry" -ge "$epoch_seconds_after" ]]; then
  echo "$line"
fi


done < aureport.txt








